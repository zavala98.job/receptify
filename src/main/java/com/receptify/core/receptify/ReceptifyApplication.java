package com.receptify.core.receptify;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(
		title = "Receptify Application",
		version = "1.0",
		description = "Receptify application for finance management purpose")
)
@SpringBootApplication
public class ReceptifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceptifyApplication.class, args);
	}

}

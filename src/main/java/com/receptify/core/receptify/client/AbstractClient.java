package com.receptify.core.receptify.client;

import com.receptify.core.receptify.log.LoggingComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;


public abstract class AbstractClient extends LoggingComponent {
	protected WebClient webClient;

	protected <T> Mono<T> call(final HttpMethod method, final String url, final Class<T> responseClass, final String body, final Map<String, String> headers, final String... params) {
		return buildRequest(method, url, body, headers)
				.uri(url, (Object[]) params)
				.retrieve()
				.toEntity(responseClass)
				.map(HttpEntity::getBody);
	}

	private WebClient.RequestBodyUriSpec buildRequest(final HttpMethod method, final String url, final String body, final Map<String, String> headers) {

		WebClient.RequestBodyUriSpec request = this.webClient.method(method);
		this.body(request, body);
		this.headers(request, headers);
		debug("Exchanging request {} - {}", method, url);
		return request;
	}

	protected void headers(final WebClient.RequestBodyUriSpec s, final Map<String, String> headers) {
		Optional.ofNullable(headers)
				.ifPresent(hs -> hs.forEach(s::header));
	}

	protected void body(final WebClient.RequestBodyUriSpec s, final String body) {
		Optional.ofNullable(body)
				.ifPresent(b -> s.body(BodyInserters.fromPublisher(Mono.just(body), String.class)));
	}
	protected Map<String, String> defaultHeaders() {
		return Map.of("Content-Type", "application/json");
	}

}

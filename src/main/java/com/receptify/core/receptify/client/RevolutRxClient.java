package com.receptify.core.receptify.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.function.Supplier;

@Component
public class RevolutRxClient extends RxClient{

	public RevolutRxClient(String baseUrl) {
		super(baseUrl);
	}
/*
	public Mono<String> get(String name) {

		return this.webClient.get().uri("/{name}/details", name)
				.retrieve().bodyToMono(Details.class);
	}
	*/
}

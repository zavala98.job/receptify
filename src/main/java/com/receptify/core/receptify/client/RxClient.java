package com.receptify.core.receptify.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.function.Supplier;

public class RxClient extends AbstractClient {


	public RxClient(String baseUrl) {
		this.webClient = WebClient.create(baseUrl);
	}

}

package com.receptify.core.receptify.config;

import com.receptify.core.receptify.client.RevolutRxClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {


	@Value("${REVOLUT.API.BASEURL}")
	private String revolutBaseUrl;

	@Value("${REVOLUT.API.KEY}")
	private String revolutApiKey;

	@Value("${REVOLUT.API.ENVIRONMENT}")
	private String revolutEnvironment;


	@Bean
	public RevolutRxClient revolutRxClient() {
		FabrickMethods fabrickMethods = FabrickMethods.builder().fabrickBase(fabrickBase).tenant(tenant).build();
		return new FabrickClientRx(fabrickMethods, apiKey, environment, proxyConfiguration);
	}
}

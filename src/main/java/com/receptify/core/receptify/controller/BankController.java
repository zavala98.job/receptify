package com.receptify.core.receptify.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class BankController extends BaseController {


	@GetMapping("transactions")
	@ResponseBody
	public Mono<String> transactions(){
		return bankService.transactions();
	}
}

package com.receptify.core.receptify.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingComponent {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	protected final void info(final String message, final Object... parameters) {
		log.info(message, parameters);
	}

	protected final void debug(final String message, final Object... parameters) {
		log.debug(message, parameters);
	}

	protected final void warn(final String message, final Object... parameters) {
		log.warn(message, parameters);
	}

	protected final void error(final String message, final Object... parameters) {
		log.error(message, parameters);
	}

	protected final void error(final Throwable t, final String message, final Object... parameters) {
		log.error(message, parameters, t);
	}


}

package com.receptify.core.receptify.service;

import com.receptify.core.receptify.repository.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

public class BaseService {

	@Autowired
	protected BankRepository bankRepository;

	public Mono<String> transactions(){
		return Mono.just("transactions");
	}
}
